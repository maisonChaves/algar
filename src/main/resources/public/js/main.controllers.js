(function () {

    'use strict';

    angular.module('pokeApp').controller('MainController', MainController);

    MainController.$inject = ['PokeService', 'TimeService', 'PokemonService', '$routeParams', '$location', 'notify', '$scope'];

    /**
     * @namespace MainController
     * @desc Main Controller of PokeApp
     * @memberOf PokeApp
     */
    function MainController(PokeService, TimeService, PokemonService, $routeParams, $location, notify, $scope) {

        var main = this;

        main.adicionarHabilidade = adicionarHabilidade;
        main.atualizarPessoas = atualizarPessoas;
        main.salvarTimes = salvarTimes;
        main.salvarPokemons = salvarPokemons;
        main.removerTime = removerTime;
        main.pokemons = [];
        main.verTime = verTime;
        main.verPokemon = verPokemon;

        listaPokemons();
        listaTimes();

        /**
         * @name adicionarHabilidade
         * @desc Adiciona habilidade a um pokemon
         * @memberOf PokeApp.MainController
         */
        function adicionarHabilidade() {
            if (!main.pokemon.habilidades) {
                main.pokemon.habilidades = [];
            }
            if (main.pokemon.habilidades.length < 4) {
                var habilidade = {
                    nome: main.habilidade.move.name
                };
                main.pokemon.habilidades.push(habilidade);
            } else {
                notify({
                    message: 'Não é possivel ter mais de 4 habilidades!',
                    classes: 'notification is-danger',
                    position: 'right'
                });
            }
        }

        /**
         * @name atualizarPessoas
         * @desc Atualiza pessoas via API
         * @memberOf PessoaApp.MainController
         */
        function atualizarPessoas() {
            if (main.pessoaForm.$valid) {
                PessoaService.update({ id: main.pessoa.id }, main.pessoa, function (pessoa) {

                    var index = main.pessoas.findIndex(byId);
                    main.pessoas[index] = main.pessoa;

                    notify({
                        message: 'Pessoa alterada com sucesso!',
                        classes: 'notification is-success',
                        position: 'right'
                    });
                    main.pessoa = {};
                    $location.path('/');
                });
            } else {
                notify({
                    message: 'Verifique o formulario',
                    classes: 'notification is-danger',
                    position: 'right'
                });
            }
        }

        function byId(item) {
            return item.id === main.time.id;
        }

        /**
         * @name carregaTime
         * @desc Recupera um time via API
         * @memberOf PokeApp.MainController
         */
        function carregaTime() {
            TimeService.get({ id: $routeParams.id }, function (time) {
                main.time = time;
            });
        }

        /**
         * @name listaPokemons
         * @desc Recupera pokemons via PokeAPI
         * @memberOf PokeApp.MainController
         */
        function listaPokemons() {
            PokeService.listaPokemons().then(function (retorno) {
                main.pokemons = retorno.data.results;
            });
        }

        /**
         * @name time
         * @desc Tela de edição de time
         * @memberOf PokeApp.MainController
         */
        function verTime() {
            $location.path('/time/' + main.time.id);
        }

        /**
         * @name time
         * @desc Tela de edição de time
         * @memberOf PokeApp.MainController
         */
        function verPokemon() {
            PokeService.verPokemon(main.pokemon.name).then(function (retorno) {
                main.pokemon = {};
                main.pokemon.nome = retorno.data.name;
                main.pokemon.sprite = retorno.data.sprites.front_default;
                main.pokemon.time = main.time;
                main.moves = retorno.data.moves;
                main.sprite = retorno.data.sprites.front_default
            });
        }

        /**
         * @name listaTimes
         * @desc Recupera times via API
         * @memberOf PokeApp.MainController
         */
        function listaTimes() {
            TimeService.query(function (retorno) {
                main.times = retorno;
            });
        }

        /**
         * @name removerTime
         * @desc Remove time via API
         * @param {Number} index index da time a ser removida
         * @memberOf PokeApp.MainController
         */
        function removerTime(index) {

            var id = main.time[index].id;

            TimesService.remove({ id: id }, function (time) {
                main.times.splice(index, 1);
                notify({
                    message: 'Time removido com sucesso!',
                    classes: 'notification is-success',
                    position: 'right'
                });
            });
        }

        /**
         * @name salvarPokemon
         * @desc Insere pokemons no times via API
         * @memberOf PokeApp.MainController
         */
        function salvarPokemons() {
            if (main.time.pokemons.length < 6) {
                PokemonService.save(main.pokemon, function (time) {
                    main.time.pokemons.push(time);
                    notify({
                        message: 'Pokemon incluido com sucesso!',
                        classes: 'notification is-success',
                        position: 'right'
                    });
                    main.pokemon = {};
                });
            } else {
                notify({
                    message: 'Não é possivel ter mais de 6 pokemons!',
                    classes: 'notification is-danger',
                    position: 'right'
                });
            }
        }

        /**
         * @name salvarTime
         * @desc Insere novos times via API
         * @memberOf PokeApp.MainController
         */
        function salvarTimes() {
            if (main.timeForm.$valid) {
                TimeService.save(main.time, function (time) {
                    main.times.push(time);
                    notify({
                        message: 'Time salvo com sucesso!',
                        classes: 'notification is-success',
                        position: 'right'
                    });
                    main.time = time;
                    verTime()
                });
            } else {
                notify({
                    message: 'Nome do time é obrigatório',
                    classes: 'notification is-danger',
                    position: 'right'
                });
            }
        }

        $scope.$on('$viewContentLoaded', function () {
            if ($routeParams.id) {
                carregaTime();
            }
        });

    }

}());