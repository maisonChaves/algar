/**
 * @namespace PokeApp
 * @desc PokeApp module
 */
(function () {
    'use strict';

    angular.module('pokeApp', ['ngRoute', 'ngResource', 'cgNotify']);
}());