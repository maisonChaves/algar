(function () {

    'use strict';

    angular.module('pokeApp').factory('PokeService', PokeService);

    PokeService.$inject = ['$http'];

    /**
     * @namespace PokeService
     * @desc Service for PokeAPI
     * @memberOf PokeApp
     */
    function PokeService($http) {

        var main = this;

        main.listaPokemons = listaPokemons;
        main.verPokemon = verPokemon;

        return main;

        function listaPokemons() {
            return $http.get('http://pokeapi.co/api/v2/pokemon/?limit=150');
        }

        function verPokemon(nome) {
            return $http.get('http://pokeapi.co/api/v2/pokemon/' + nome);
        }
    };

}());