(function () {

    'use strict';

    angular.module('pokeApp').factory('TimeService', TimeService);

    TimeService.$inject = ['$resource'];

    /**
     * @namespace TimeService
     * @desc Service for time API
     * @memberOf PokeApp
     */
    function TimeService($resource) {
        return $resource('/api/time/:id', null, { 'update': { method: 'PUT' } });
    };

}());