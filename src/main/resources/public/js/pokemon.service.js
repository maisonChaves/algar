(function () {

    'use strict';

    angular.module('pokeApp').factory('PokemonService', PokemonService);

    PokemonService.$inject = ['$resource'];

    /**
     * @namespace PokemonService
     * @desc Service for pokemon API
     * @memberOf PokeApp
     */
    function PokemonService($resource) {
        return $resource('/api/pokemon/:id', null, { 'update': { method: 'PUT' } });
    };

}());