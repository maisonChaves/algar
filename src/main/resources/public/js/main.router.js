(function () {

    'use strict';

    angular.module('pokeApp').config(RouterConfig);

    RouterConfig.$inject = ['$routeProvider', '$locationProvider'];

    /**
     * @namespace RouterConfig
     * @desc Router configuraion of Poke App
     * @memberOf PokeApp
     */
    function RouterConfig($routeProvider, $locationProvider) {

        $locationProvider.hashPrefix('');

        $routeProvider.when('/', {
            templateUrl: 'views/times.html',
            controller: 'MainController'
        });
        $routeProvider.when('/time/:id', {
            templateUrl: 'views/time.html',
            controller: 'MainController'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }

}());