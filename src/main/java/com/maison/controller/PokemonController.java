package com.maison.controller;

import com.maison.model.Pokemon;
import com.maison.service.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;

@Component
@Path("/pokemon")
public class PokemonController {

    @Context
    private UriInfo uriInfo;

    @Autowired
    private PokemonService pokemonService;

    @Context
    private ResourceContext resourceContext;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {

        Pokemon pokemon = pokemonService.getById(id);

        return Response.ok().entity(pokemon).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Pokemon> list() {
        return pokemonService.list();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Pokemon pokemon) {

        pokemonService.create(pokemon);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", pokemon.getId())
                .build();

        return Response.created(location).entity(pokemon).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, Pokemon pokemon) {
        pokemon.setId(id);
        pokemonService.update(pokemon);

        return Response.ok().entity(pokemon).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        pokemonService.delete(id);
        return Response.accepted().build();
    }
}
