package com.maison.controller;

import com.maison.model.Time;
import com.maison.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;

@Component
@Path("/time")
public class TimeController {

    @Context
    private UriInfo uriInfo;

    @Autowired
    private TimeService timeService;

    @Context
    private ResourceContext resourceContext;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {

        Time time = timeService.getById(id);

        return Response.ok().entity(time).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Time> list() {
        return timeService.list();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Time time) {

        timeService.create(time);

        URI location = uriInfo.getAbsolutePathBuilder()
                .path("{id}")
                .resolveTemplate("id", time.getId())
                .build();

        return Response.created(location).entity(time).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, Time time) {
        time.setId(id);
        timeService.update(time);

        return Response.ok().entity(time).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        timeService.delete(id);
        return Response.accepted().build();
    }
}
