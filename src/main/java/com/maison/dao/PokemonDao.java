package com.maison.dao;

import com.maison.model.Pokemon;

import java.util.Collection;

public interface PokemonDao {

    void create(Pokemon pokemon);

    void update(Pokemon pokemon);

    Pokemon getById(long id);

    boolean checkExistence(Pokemon pokemon);

    void delete(long id);

    Collection<Pokemon> list();
}
