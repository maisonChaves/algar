package com.maison.dao;

import java.util.Collection;

import com.maison.model.Time;

public interface TimeDao {

    void create(Time time);

    void update(Time time);

    Time getById(long id);

    boolean checkExistence(Time time);

    void delete(long id);

    Collection<Time> list();
}
