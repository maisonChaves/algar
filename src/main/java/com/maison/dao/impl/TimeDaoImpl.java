package com.maison.dao.impl;

import com.maison.dao.TimeDao;
import com.maison.model.Time;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class TimeDaoImpl implements TimeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Time time) {
        entityManager.persist(time);
    }

    @Override
    public void update(Time time) {
        entityManager.merge(time);
    }

    @Override
    public Time getById(long id) {
        return entityManager.find(Time.class, id);
    }

    @Override
    public boolean checkExistence(Time time) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Time> query = cb.createQuery(Time.class);

        ParameterExpression<String> parameter = cb.parameter(String.class, "name");

        Root<Time> times = query.from(Time.class);

        query.where(cb.like(times.get("name"), parameter));

        return !entityManager
                .createQuery(query)
                .setParameter(parameter, time.getNome())
                .getResultList().isEmpty();
    }

    @Override
    public void delete(long id) {
        Time time = getById(id);
        if (time != null) {
            entityManager.remove(time);
        }
    }

    @Override
    public Collection<Time> list() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Time> query = cb.createQuery(Time.class);

        Root<Time> c = query.from(Time.class);
        query.select(c);

        return entityManager.createQuery(query).getResultList();
    }

}
