package com.maison.dao.impl;

import com.maison.dao.PokemonDao;
import com.maison.model.Pokemon;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public class PokemonDaoImpl implements PokemonDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Pokemon pokemon) {
        entityManager.persist(pokemon);
    }

    @Override
    public void update(Pokemon pokemon) {
        entityManager.merge(pokemon);
    }

    @Override
    public Pokemon getById(long id) {
        return entityManager.find(Pokemon.class, id);
    }

    @Override
    public boolean checkExistence(Pokemon pokemon) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Pokemon> query = cb.createQuery(Pokemon.class);

        ParameterExpression<String> parameter = cb.parameter(String.class, "name");

        Root<Pokemon> times = query.from(Pokemon.class);

        query.where(cb.like(times.get("nome"), parameter));

        return !entityManager
                .createQuery(query)
                .setParameter(parameter, pokemon.getNome())
                .getResultList().isEmpty();
    }

    @Override
    public void delete(long id) {
        Pokemon pokemon = getById(id);
        if (pokemon != null) {
            entityManager.remove(pokemon);
        }
    }

    @Override
    public Collection<Pokemon> list() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Pokemon> query = cb.createQuery(Pokemon.class);

        Root<Pokemon> c = query.from(Pokemon.class);
        query.select(c);

        return entityManager.createQuery(query).getResultList();
    }

}
