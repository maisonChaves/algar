package com.maison.service.impl;

import com.maison.dao.TimeDao;
import com.maison.model.Time;
import com.maison.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class TimeServiceImpl implements TimeService {
    @Autowired
    private TimeDao timeDao;

    @Override
    public void create(Time time) {
        timeDao.create(time);
    }

    @Override
    public Collection<Time> list() {
        return timeDao.list();
    }

    @Override
    public void update(Time time) {
        timeDao.update(time);
    }

    @Override
    public Time getById(long id) {
        return timeDao.getById(id);
    }

    @Override
    public void delete(long id) {
        timeDao.delete(id);
    }
}
