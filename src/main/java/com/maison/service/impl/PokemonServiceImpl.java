package com.maison.service.impl;

import com.maison.dao.PokemonDao;
import com.maison.model.Habilidade;
import com.maison.model.Pokemon;
import com.maison.service.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class PokemonServiceImpl implements PokemonService {
    @Autowired
    private PokemonDao pokemonDao;

    @Override
    public void create(Pokemon pokemon) {

        for(Habilidade habilidade: pokemon.getHabilidades()){
            habilidade.setPokemon(pokemon);
        }

        pokemonDao.create(pokemon);
    }

    @Override
    public Collection<Pokemon> list() {
        return pokemonDao.list();
    }

    @Override
    public void update(Pokemon pokemon) {
        pokemonDao.update(pokemon);
    }

    @Override
    public Pokemon getById(long id) {
        return pokemonDao.getById(id);
    }

    @Override
    public void delete(long id) {
        pokemonDao.delete(id);
    }
}
