package com.maison.service;

import com.maison.model.Time;

import java.util.Collection;

public interface TimeService {

    void create(Time time);

    Collection<Time> list();

    void update(Time time);

    Time getById(long id);

    void delete(long id);

}
