package com.maison.service;

import com.maison.model.Pokemon;

import java.util.Collection;

public interface PokemonService {

    void create(Pokemon pokemon);

    Collection<Pokemon> list();

    void update(Pokemon pokemon);

    Pokemon getById(long id);

    void delete(long id);

}
