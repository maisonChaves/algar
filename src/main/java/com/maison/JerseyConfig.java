package com.maison;

import com.maison.controller.PokemonController;
import com.maison.controller.TimeController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(TimeController.class);
        register(PokemonController.class);
    }
}
