package com.maison.model;

import javax.persistence.*;

/**
 * Created by maison on 14/07/17.
 */
@Entity
@Table(name = "USUARIO")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String nome;

    @Column(nullable = false, unique = true)
    String email;
}
