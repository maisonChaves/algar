# Objetivo

O objetivo desse desafio é desenvolver uma interface para que treinadores possam montar seu time de Pokémon. 
* O time deve ter no máximo 6 criaturas. 
* Deve ser possível escolher criaturas a partir de uma lista, e após selecionada uma criatura deve ser possível escolher quais habilidades (Moves) ela terá, limitado a 4 habilidades distintas. 
* A compatibilidade das habilidades e dos Pokémon não precisa ser tratadas, um Pokémon pode ter as habilidades que o treinador desejar.


## Itens desejáveis 
(Não sendo todos obrigatórios)

* Aplicação multiusuário
* Ser possível filtrar Pokémon pelo nome.
* Ser possível filtrar habilidades pelo nome.
* Permitir a criação e manutenção de 1 a n times.
* Restringir que um treinador veja somente os seus próprios times.
 

É sua responsabilidade determinar e aplicar critérios de qualidade para preenchimento de campos, tratamento de exceções, feedback ao usuário e outros que achar pertinente.

## Tecnologias recomendadas

* Frontend - AngularJS
* Backend - (Opcional) Java ou PHP.
* Banco de dados - (Opcional) 
* Ferramenta de build - Maven, Graddle, NPM ou o de sua preferência
 

## Entrega

Para realizar a entrega, disponibilizar um repositório git com a aplicação, juntamente de instruções COMPLETAS de como executá-lo.

Comando maven pra executar o projeto.

```
mvn spring-boot:run
```
Acessar a url
```
http://localhost:8080
```

## Pokéapi

Pokéapi é uma api grátis e aberta que provê serviços que permitem consumir informações diversas relacionadas a Pokémon.

No site https://pokeapi.co/ é possível consultar a documentação de como funciona essa API.
